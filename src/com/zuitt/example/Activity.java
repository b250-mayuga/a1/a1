package com.zuitt.example;

import java.util.Scanner;
public class Activity {

    public static void main(String[] args) {
        Scanner myObject = new Scanner(System.in);

        System.out.println("First Name:");
        String firstname = myObject.nextLine();

        System.out.println("Last Name:");
        String lastname = myObject.nextLine();

        System.out.println("First subject grade:");
        double grade1 = myObject.nextDouble();

        System.out.println("Second subject grade:");
        double grade2 = myObject.nextDouble();

        System.out.println("Third subject grade:");
        double grade3 = myObject.nextDouble();

        double average = (grade1+grade2+grade3)/3;
        System.out.println("Good day, " + firstname + " " + lastname);
        System.out.println("Your grade is " + Math.round(average));
    }

}
